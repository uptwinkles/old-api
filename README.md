# Uptwinkles API
Get data about Uptwinkles. The API is located at [api.uptwinkles.co](https://api.uptwinkles.co).

You can query:

* [`/chores`](https://api.uptwinkles.co/chores) - list all chores
* [`/chores/now`](https://api.uptwinkles.co/chores/now) - show current chore assignments
* [`/people`](https://api.uptwinkles.co/people) - list all people
* [`/dinner`](https://api.uptwinkles.co/dinner) - suggested dinner and cook

**Warning:** This API is incomplete and subject to change dramatically.

## Local development

Ensure that Python and virtualenvwrapper are installed.

First time:
```
mkvirtualenv --python=python3 uptwinkles-api
workon uptwinkles-api
pip install -r requirements.txt

```

Each time:
```
workon uptwinkles-api
python app/server.py
```

## License
uptwinkles-api is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
