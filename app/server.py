from flask import Flask, jsonify
from flask_cors import CORS
from datetime import datetime
import math
import time
import random

app = Flask(__name__)
CORS(app)

chores = ["wash the dishes", "take out the trash", "clean the kitchen", "sweep the house"]
people = ["Alex", "Chad", "Mary Kate", "Jacqueline"]
dinners = [
  "rice with tempeh and peanut sauce",
  "tempeh stir fry",
  "tikka masala chickpeas over rice",
  "tacos",
  "tofu scramble",
  "bananas",
  "chickpeas and rice with tahini sauce",
  "pasta with lemon butter sauce",
  "pizza",
  "veggie burgers",
  "tofu stir fry",
  "pasta salad"
]

today = (datetime.now() - datetime(1970,1,1)).days
dinner_today = random.choice(dinners)

def get_dinner_today():
    global today
    global dinner_today
    day = (datetime.now() - datetime(1970,1,1)).days
    if day > today:
        dinner_today = random.choice(dinners)
        today = day
    return dinner_today

@app.route("/")
def main():
    return "Welcome to the Uptwinkles API! Try /chores, /people, or /chores/now."

@app.route("/chores")
def list_chores():
    return jsonify(chores)

@app.route("/chores/now")
def chores_now():
    # Number of weeks since epoch
    offset = math.floor(((datetime.now() - datetime(1970,1,1)).days + 3) // 7)

    assignments = []
    for n, chore in enumerate(chores):
        index = (n + offset) % len(people)
        person = people[index]
        assignments.append({
            "person": person,
            "chore": chore
        })

    return jsonify(assignments)

@app.route("/people")
def list_people():
    return jsonify(people)

@app.route("/dinner")
def dinner_assignment():
    # Number of days since epoch
    day = (datetime.now() - datetime(1970,1,1)).days
    offset = day % len(people)
    print(today)
    return jsonify({
        "person": people[offset],
        "dinner": get_dinner_today()
    })

if __name__ == "__main__":
    app.run(host='0.0.0.0')
